Source: nsync
Section: science
Homepage: https://github.com/google/nsync
Priority: optional
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/science-team/nsync.git
Vcs-Browser: https://salsa.debian.org/science-team/nsync
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Mo Zhou <lumin@debian.org>
Build-Depends: cmake, debhelper-compat (= 13)

Package: libnsync1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C library that exports various synchronization primitives (C lib)
 nsync is a C library that exports various synchronization primitives:
 .
  * locks
  * condition variables
  * run-once initialization
  * waitable counter (useful for barriers)
  * waitable bit (useful for cancellation, or other conditions)
 .
 This package ships C shared object.

Package: libnsync-cpp1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C library that exports various synchronization primitives (C++ lib)
 nsync is a C library that exports various synchronization primitives:
 .
  * locks
  * condition variables
  * run-once initialization
  * waitable counter (useful for barriers)
  * waitable bit (useful for cancellation, or other conditions)
 .
 This package ships C++ shared object.

Package: libnsync-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libnsync-cpp1 (= ${binary:Version}),
         libnsync1 (= ${binary:Version}),
         ${misc:Depends}
Description: C library that exports various synchronization primitives (dev)
 nsync is a C library that exports various synchronization primitives:
 .
  * locks
  * condition variables
  * run-once initialization
  * waitable counter (useful for barriers)
  * waitable bit (useful for cancellation, or other conditions)
 .
 This package ships header files.
